class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            mistakes: 0,
            correctAnswers: 0,
            exercisesLeft: 15
        };
        
        this.exerciseDone = this.exerciseDone.bind(this);
        this.resetGame = this.resetGame.bind(this);
    }

    exerciseDone(correct) {
        console.log("exerciseDone: " + correct);
        if (!correct) {
            this.state.mistakes = ++this.state.mistakes;
        } else {
            this.state.correctAnswers = ++this.state.correctAnswers;
        }
        this.state.exercisesLeft = --this.state.exercisesLeft;
        this.setState(this.state);
        console.log("mistakes: " + this.state.mistakes);
        console.log("exercisesLeft: " + this.state.exercisesLeft);
    }

    resetGame() {
        this.state.mistakes = 0;
        this.state.correctAnswers = 0;
        this.state.exercisesLeft = 15;
        this.setState(this.state);
    }

    render() {
        return <div className="jumbotron jumbotron-fluid">
            { this.state.exercisesLeft != 0 ? <Exercise onExcerciseDone={this.exerciseDone}></Exercise> : null }
            <AnswerCounter mistakes={this.state.mistakes} correctAnswers={this.state.correctAnswers}></AnswerCounter>
            { this.state.exercisesLeft == 0 ? <ResetButton resetGame={this.resetGame}></ResetButton> : null }
        </div>
    }
}

class ResetButton extends React.Component {
    render() {
        return <button type="button" className="btn btn-secondary btn-lg btn-block" onClick={() => this.props.resetGame()}>Noch mal probieren</button>
    }
}

class AnswerCounter extends React.Component {

    render() {
        return <div className="container">
            <h1 className="display-4">Richtig: {this.props.correctAnswers}</h1>
            <h1 className="display-4">Falsch: {this.props.mistakes}</h1>
        </div>
    }
}

class Exercise extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            operator: '',
            operatorToShow: '',
            first: '',
            second: '',
            rightAnswer: ''
        };
        this.randomizeExercise();
        this.answerClicked = this.answerClicked.bind(this);
    }

    randomizeExercise() {
        this.state.operator = Math.ceil(Math.random() * 4);
        if (this.state.operator == 1) {
            this.state.operatorToShow  = '+';
            this.state.first = Math.ceil(Math.random() * 100);
            this.state.second = Math.ceil(Math.random() * 100);
            this.state.rightAnswer = this.state.first + this.state.second;
        } else if (this.state.operator == 2) {
            this.state.operatorToShow  = '-';
            this.state.first = Math.ceil(Math.random() * 99) + 1;
            this.state.second = Math.ceil(Math.random() * this.state.first);
            this.state.rightAnswer = this.state.first - this.state.second;
        } else if (this.state.operator == 3) {
            this.state.operatorToShow  = '*';
            this.state.first = this.randomUpper5();
            this.state.second = this.randomUpper5();
            this.state.rightAnswer = this.state.first * this.state.second;
        } else if (this.state.operator == 4) {
            this.state.operatorToShow  = '/';
            var blah = this.randomUpper5();
            this.state.second = this.randomUpper5();
            this.state.first = blah * this.state.second;
            this.state.rightAnswer = blah;
        }
    }

    randomUpper5() {
        if (Math.ceil(Math.random() * 2) == 2) {
            return Math.ceil(Math.random() * 5) + 5;
        } else {
            return Math.ceil(Math.random() * 10);
        }
    }

    answerClicked(correct) {
        this.props.onExcerciseDone(correct);
        this.randomizeExercise();
        this.setState(this.state);
    }

    render() {

        return <div className="container">
            <h1 className="display-2">{this.state.first} {this.state.operatorToShow} {this.state.second} </h1>
            <p className="lead">ist gleich: </p>

            <ListOfAnswers onAnswerClicked={this.answerClicked} rightAnswer={this.state.rightAnswer}></ListOfAnswers>

        </div>

    }


}


class ListOfAnswers extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            answers: [],
            selectedAnswer: '',
            correctAnswerSelected: false
        };
        this.prepareAnswers();
    }

    prepareAnswers() {
        this.state.answers[0] = this.props.rightAnswer;

        var m = 5;  // total elements 4 + 1 = 5
        while (m) {
            if (this.props.rightAnswer > 100) {
                var i = Math.ceil(Math.random() * 180);
            } else {
                var i = Math.ceil(Math.random() * 100);
            }
            
            
            if (!this.state.answers.includes(i)) {
                this.state.answers.push(i);
                m--;
            }

        }

        this.shuffle();
    }

    shuffle() {
        var m = this.state.answers.length, t, i;

        // While there remain elements to shuffle…
        while (m) {

            // Pick a remaining element…
            i = Math.floor(Math.random() * m--);

            // And swap it with the current element.
            t = this.state.answers[m];
            this.state.answers[m] = this.state.answers[i];
            this.state.answers[i] = t;
        }
    }

    render() {
        return <ul className="list-group">
            {this.state.answers.map(i =>
                <li key={i}
                    className={this.state.selectedAnswer === i ? this.state.correctAnswerSelected ? 'list-group-item list-group-item-success' : 'list-group-item list-group-item-danger' : 'list-group-item'}
                    onClick={() => this.handleAnswer(i)}>
                    {i}
                </li>
            )}
        </ul>
    }

    handleAnswer(answer) {
        if (this.state.selectedAnswer === '') {
            this.state.selectedAnswer = answer;
            if (answer === this.props.rightAnswer) {
                this.state.correctAnswerSelected = true;
            } else {
                this.state.correctAnswerSelected = false;
            }

            this.setState(this.state);
        }
    }

    async componentDidUpdate() {
        if (this.state.selectedAnswer != '') {

            await this.sleep(1500);
            this.state.selectedAnswer = '';
            this.props.onAnswerClicked(this.state.correctAnswerSelected);

            this.state.answers = [];
            this.state.correctAnswerSelected = false;

            this.prepareAnswers();
            this.forceUpdate();
        }
    }

    sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
}
